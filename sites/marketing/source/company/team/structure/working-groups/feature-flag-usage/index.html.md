---
layout: markdown_page
title: "Feature Flag usage Working Group"
description: "The feature flag usage working group aims to establish global policies and processes around the usage of feature flags in the development of GitLab"
canonical_path: "/company/team/structure/working-groups/feature-flag-usage/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value                                                                                                                  |
|:-------------|:-----------------------------------------------------------------------------------------------------------------------|
| Date Created | Dec 3, 2020                                                                                                            |
| End Date     | TBD                                                                                                                    |
| Slack        | [#wg_feature-flag-usage](https://gitlab.slack.com/archives/C01GACLFVT3)      (only accessible from within the company) |
| Google Doc   | [Working Group Agenda](https://docs.google.com/document/d/1Q_GyFSMep0SXGxnNW_PgrX2Xzq6EOOx7ZFbtyR_9DvY/edit#) (only accessible from within the company)  |

### Charter

This working group will co-ordinate the organization of the effort to improve the usage of feature flags in the development of GitLab. There are many asynchronous and currently ongoing discussions in the organization about internal feature flag usage. We aim to collect and co-ordinate these conversations in order to create uniform policies and processes for the usage of feature flags within GitLab. The uniformity of these policies is key in order for internal stakeholders, community members, and customers have more consistent insight into the availiabilty of GitLab features.

### Scope and Definitions

This group will create processes and policies that are as lean as possible in order to ensure that the way feature flags are used by engineers meets the needs of all stakeholders. Stakeholders for feature flags generally are individuals who care about the current state of features on GitLab.com and self-hosted GitLab instances of a particular version.

#### Definitions
* **Feature Flag** - this isn't necessarily the [feature flag feature](https://docs.gitlab.com/ee/operations/feature_flags.html) but rather the way we [use feature flags in the development of GitLab](https://docs.gitlab.com/ee/development/feature_flags/index.html)

### Exit Criteria

* Fulfillment of the feature flag [architectural blueprint](https://docs.gitlab.com/ee/architecture/blueprints/feature_flags_development/)
* Completion of the [internal usage of feature flags epic](https://gitlab.com/groups/gitlab-org/-/epics/3551)
* Refinement and assignment of [Feature Flag Training](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/80476d9861756b3a9c8a062267288f36ff6156ca/.gitlab/issue_templates/feature-flag-training.md), and [Feature Flag Monitoring training](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/80476d9861756b3a9c8a062267288f36ff6156ca/.gitlab/issue_templates/monitoring-training.md) for GitLab engineers
* Audit, refine, and communicate the [Feature flags in development of GitLab documentation](https://docs.gitlab.com/ee/development/feature_flags/)
* Each functional lead is satisfied of the state of the feature flag processes in place in the development of GitLab
* Provide data to support long-term feature flag management so we can monitor the lifecycle of feature flags and take action on them as appropriate.

## Roles and Responsibilities

The functional leads will be repsonsible for:

* Representing the needs of individual stakeholders in their department/sub-dept
* Gathering and consolidating feedback on specific proposals from their department/sub-dept
* Communicating the output from the working group (if any) and answering questions from their dept/sub-dept

Ideally the functional lead is someone who is an IC that might be affected by the policy put in place. but anyone capable of representing a department or sub-department in the fashion mentioned above is welcome.

The stakeholder departments in the table are the ones identified in the [architectural blueprint](https://docs.gitlab.com/ee/architecture/blueprints/feature_flags_development/).

| Working Group Role                     | Person | Title |
|:---------------------------------------|:-------|:------|
| Executive Sponsor                      | Christopher Lefelhocz | Senior Director of Development           |
| Facilitator                            | Darby Frey | Senior Engineering Manager, Verify                  |
| Functional Lead                        | Ricky Wiens | Backend Engineering Manager, Verify:Testing        |
| Functional Lead                        | Kamil Trzciński | Distinguished Engineer, Ops and Enablement     |
| Functional Lead                        | Anthony Sandoval | Engineering Manager, Reliability              |
| Functional Lead                        | James Heimbuck | Senior Product Manager, Verify:Testing          |
| Member                                 | Grzegorz Bizon | Staff Backend Engineer, Verify                  |
| Member                                 | Craig Gomes | Backend Engineering Manager, Memory and Database   |
| Member                                 | Michelle Gill | Engineering Manager, Create:Source Code          |
| Member                                 | Wayne Haber | Director of Engineering, Threat Management         |
| Member                                 | Doug Stull | Senior Fullstack Engineer, Growth:Expansion         |
| Member                                 | Andrew Fontaine | Senior Frontend Engineer, Release              |
| Member                                 | Rémy Coutable | Staff Backend Engineer, Engineering Productivity |
